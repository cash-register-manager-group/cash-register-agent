from flask import Flask
from flask_restful import Api

import setting
from app.routers import add_routers
import logging

if __name__ == '__main__':
    logging.basicConfig(filename=setting.LOG_FILE_PATH, level=logging.DEBUG)
    app = Flask(__name__)
    api = Api(app)
    add_routers(api)
    app.run(host=setting.SERVER_IP, port=setting.SERVER_PORT, debug=setting.IS_DEBUG)
