from time import sleep

from app.service import CashRegister

if __name__ == '__main__':
    cash_register = CashRegister.get_instance()
    print('The X report is printing ...')
    cash_register.report_x()
    sleep(1)
    print('Finish')
