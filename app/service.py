import datetime

from serial import Serial

from app.device.cr.krypton_transport import KryptonTransport
from app.device.cr.krypton_worker import KryptonWorker
from app.device.cr.log import LogPrint
from setting import SERIAL_PORT


class CashRegister:
    _instance = None

    def __init__(self, log=LogPrint()):
        self._log = log
        transport = KryptonTransport(Serial(port=SERIAL_PORT))
        self._krypton = KryptonWorker(transport, log=self._log)

    @staticmethod
    def get_instance(log=LogPrint()):
        if CashRegister._instance is None:
            CashRegister._instance = CashRegister(log)
        return CashRegister._instance

    # amount is in hryvnia
    def payment(self, amount_hryvnia: int, goods_name: str, goods_code: int = 1, is_bank=False, txt=None):
        self._krypton.payment(amount_hryvnia, goods_name, goods_code, is_bank, txt=txt)

    def report_x(self):
        self._krypton.report_x()

    def report_z(self):
        self._krypton.report_z()

    def dtime(self, dtime: datetime.datetime = None):
        return self._krypton.dtime(dtime)

    def cash_in_box(self):
        return self._krypton.cash_in_box()

    def payments(self):
        return self._krypton.payments()

    def tax_rates(self):
        return self._krypton.tax_rates()

    def payment_params(self):
        return self._krypton.payment_params()

