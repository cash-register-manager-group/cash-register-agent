import datetime
import random

from serial import Serial

from app.device.cr import commands
from app.device.cr.krypton_transport import KryptonTransport
from app.device.cr.log import LogPrint, LogInterface


class KryptonWorkerBase:
    def __init__(self, transport: KryptonTransport):
        self._transport = transport
        self._pwd = '000000'
        self._seq = 1

    @property
    def password(self):
        return self._pwd

    @password.setter
    def password(self, val):
        self._pwd = val

    @property
    def seq(self):
        return self._seq

    @seq.setter
    def seq(self, val):
        self._seq = val


class KryptonWorker(KryptonWorkerBase):
    TAG = 'KryptonWorker'
    TYPE_ID_CASH = 0
    TYPE_ID_BANK = 2

    def __init__(self, transport: KryptonTransport, log=LogInterface(), seq=None):
        super().__init__(transport)
        self._log = log
        if seq is not None:
            self.seq = seq
        else:
            self.seq = random.randint(10, 99)

    def print_comment(self, txt):
        self._txt_open()
        self._txt(txt)
        self._invoice_close()

    def payment(self, amount: int, good_name: str, goods_code: int = 1, is_bank=False, txt=None):
        self._log.debug(
            "{}.payment: amount={}, good_name={}, goods_code={}, is_bank={}, txt={}, seq={}"
                .format(self.TAG, amount, good_name, goods_code, is_bank, txt, self.seq))
        try:
            self._invoice_cancel()
        except:
            pass
        self._invoice_open()
        if txt is not None:
            self._payment_txt(txt)
        self._add_goods(good_name, goods_code)
        self._purchase_goods(amount, goods_code)
        self._print_total()
        self._payment(amount, is_bank)
        self._invoice_close()

    def report_x(self):
        self._seq_int()
        comm = commands.DailyReport(self._transport)
        comm.seq = self._seq
        comm.x()
        res = comm.exchange_data()
        self._log.debug("{}.report_x: request={}, response={}".format(self.TAG, comm.data(), res))
        return res

    def report_z(self):
        self._seq_int()
        comm = commands.DailyReport(self._transport)
        comm.seq = self._seq
        comm.z()
        res = comm.exchange_data()
        self._log.debug("{}.report_x: request={}, response={}".format(self.TAG, comm.data(), res))
        return res

    def dtime(self, dtime: datetime.datetime = None):
        if dtime is not None:
            self._seq_int()
            comm = commands.SetDate(self._transport)
            comm.dtime = dtime
            comm.seq = self._seq
            comm.exchange_data()
        self._seq_int()
        comm = commands.Date(self._transport)
        comm.seq = self._seq
        res = comm.exchange_data()
        return comm.data_to_datetime(res.data)

    def cash_in_box(self) -> [int, int]:
        self._seq_int()
        comm = commands.DaySums(self._transport)
        comm.seq = self._seq
        comm.source = 3
        comm.index = 0
        comm.modifier = 0
        res = comm.exchange_data()
        args = comm.data_to_args(res.data)
        return [float(args[0]), float(args[1])]

    def payments(self) -> [int, int]:
        params = self.payment_params()
        comm = commands.DaySums(self._transport)
        res = []
        for idx in range(0, 5):
            self._seq_int()
            comm.seq = self._seq
            comm.source = 2
            comm.index = idx
            comm.modifier = 0
            pkg = comm.exchange_data()
            args = comm.data_to_args(pkg.data)
            name = ''
            if len(params[idx]) > 1:
                name = params[idx][1]
            res.append([name, float(args[0]), float(args[1])])
        return res

    def tax_rates(self):
        comm = commands.TaxRates(self._transport)
        res = []
        for idx in range(0, 5):
            self._seq_int()
            comm.seq = self._seq
            comm.index = idx
            pkg = comm.exchange_data()
            args = comm.data_to_args(pkg.data)
            res.append(args)
        return res

    def payment_params(self):
        comm = commands.PaymentParams(self._transport)
        res = []
        for idx in range(0, 5):
            self._seq_int()
            comm.seq = self._seq
            comm.index = idx
            pkg = comm.exchange_data()
            args = comm.data_to_args(pkg.data)
            res.append(args)
        return res

    def _seq_int(self):
        if self.seq >= 223:
            self.seq = 1
        else:
            self._seq += 1

    def _payment_txt(self, txt):
        if isinstance(txt, list) or isinstance(txt, tuple) or isinstance(txt, set):
            for t in txt:
                self._txt(t)
        else:
            self._txt(txt)

    def _invoice_cancel(self):
        self._seq_int()
        comm = commands.InvoiceCancel(self._transport)
        comm.seq = self._seq
        res = comm.exchange_data()
        self._log.debug("{}._invoice_cancel: request={}, response={}".format(self.TAG, comm.data(), res))
        return res

    def _invoice_open(self):
        self._seq_int()
        comm = commands.InvoiceFiscalOpen(self._transport)
        comm.seq = self._seq
        res = comm.exchange_data()
        self._log.debug("{}._invoice_open: request={}, response={}".format(self.TAG, comm.data(), res))
        return res

    def _add_goods(self, name, code=1):
        self._seq_int()
        comm = commands.AddGoods(self._transport)
        comm.seq = self._seq
        comm.goods_code = code
        comm.name = name
        comm.department = 1
        res = comm.exchange_data()
        self._log.debug("{}._add_goods: request={}, response={}".format(self.TAG, comm.data(), res))
        return res

    def _purchase_goods(self, amount, goods_code, count=1):
        self._seq_int()
        comm = commands.PurchaseGoods(self._transport)
        comm.seq = self._seq
        comm.goods_code = goods_code
        comm.price = amount
        comm.count = count
        res = comm.exchange_data()
        self._log.debug("{}._purchase_goods: request={}, response={}".format(self.TAG, comm.data(), res))
        return res

    def _payment(self, amount, is_bank=False):
        self._seq_int()
        comm = commands.Payment(self._transport)
        comm.seq = self._seq
        comm.amount = amount
        if is_bank:
            comm.type = self.TYPE_ID_BANK
        else:
            comm.type = self.TYPE_ID_CASH
        res = comm.exchange_data()
        self._log.debug("{}._payment: request={}, response={}".format(self.TAG, comm.data(), res))
        return res

    def _print_total(self):
        self._seq_int()
        comm = commands.PrintTotal(self._transport)
        comm.seq = self._seq
        res = comm.exchange_data()
        self._log.debug("{}._print_total: request={}, response={}".format(self.TAG, comm.data(), res))
        return res

    def _txt_open(self):
        self._seq_int()
        comm_open = commands.TextDocumentOpen(self._transport)
        comm_open.seq = self._seq
        res = comm_open.exchange_data()
        self._log.debug("{}._txt_open: request={}, response={}".format(self.TAG, comm_open.data(), res))
        return res

    def _txt(self, txt):
        self._seq_int()
        comm_txt = commands.PrintComment(self._transport)
        comm_txt.seq = self._seq
        comm_txt.txt = txt
        res = comm_txt.exchange_data()
        self._log.debug("{}._txt: request={}, response={}".format(self.TAG, comm_txt.data(), res))
        return res

    def _invoice_close(self):
        self._seq_int()
        comm_close = commands.InvoiceClose(self._transport)
        comm_close.seq = self._seq
        res = comm_close.exchange_data()
        self._log.debug("{}._invoice_close: request={}, response={}".format(self.TAG, comm_close.data(), res))
        return res


if __name__ == '__main__':
    transport = KryptonTransport(Serial(port='COM4'))
    w = KryptonWorker(transport, log=LogPrint(), seq=113)
    w.payment(100, 'Послуги', 100)

    # w.report_x()
    # w.print_comment('test')
