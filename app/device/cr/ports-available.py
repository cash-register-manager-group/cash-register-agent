import asyncio
import os

from aioserial import aioserial


class AioPortsAvailable:

    @staticmethod
    def list_of_all():
        ports = []
        if os.name == 'nt':
            for n in range(1, 100):
                ports.append("COM{}".format(n))
        if os.name == 'posix':
            for n in range(0, 99):
                ports.append("/dev/ttyS{}".format(n))
        return ports

    @staticmethod
    async def list_of_available():
        ports = []
        for port_name in AioPortsAvailable.list_of_all():
            try:
                aioserial.AioSerial(port=port_name).close()
                ports.append(port_name)
            except:
                pass
        return ports


async def main():
    print(await AioPortsAvailable.list_of_available())


if __name__ == '__main__':
    asyncio.run(main())
