import asyncio
import time


async def pre_say_after(delay, what):
    i = 0
    while i < 10:
        await asyncio.sleep(delay)
        print('{} - {}'.format(what, i))
        i += 1


async def say_after(delay, what, is_pre=False):
    await asyncio.sleep(delay)
    print(what)
    if is_pre:
        await pre_say_after(1, 'pre- {}'.format(what))


async def nested():
    return 42


async def main():
    print(f"started at {time.strftime('%X')}")

    task1 = asyncio.create_task(say_after(8, 'task 1', True))
    task2 = asyncio.create_task(say_after(2, 'task 2', True))
    await task1
    await task2
    print(f"finished at {time.strftime('%X')}")


if __name__ == '__main__':
    asyncio.run(main())
