class PkgResponseEntity:
    def __init__(self):
        self._len = None
        self._seq = None
        self._cmd = None
        self._error_code = None
        self._data = []
        self._status = []

    @property
    def len(self):
        return self._len

    @len.setter
    def len(self, v):
        self._len = v

    @property
    def seq(self):
        return self._seq

    @seq.setter
    def seq(self, v):
        self._seq = v

    @property
    def cmd(self):
        return self._cmd

    @cmd.setter
    def cmd(self, v):
        self._cmd = v

    @property
    def error_code(self):
        return self._error_code

    @error_code.setter
    def error_code(self, v):
        self._error_code = self._error_code_to_int(v)

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, v):
        self._data = v

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, v):
        self._status = v

    def __repr__(self):
        return "PkgReceiverEntity: len={}, seq={}, cmd={}, error_code={}, data={}, status={}" \
            .format(self._len, self._seq, self._cmd, hex(self._error_code), self._data, self._status)

    def _error_code_to_int(self, arr: []):
        d = {'0': 0x00, '1': 0x01, '2': 0x02, '3': 0x03, '4': 0x04, '5': 0x05, '6': 0x06, '7': 0x07, '8': 0x08,
             '9': 0x09,
             'A': 0x0a, 'B': 0x0b, 'C': 0x0c, 'D': 0x0d, 'E': 0x0e, 'F': 0x0f}
        if len(arr) != 4:
            raise ValueError('len of arr should be 4')
        d0 = d.get(chr(arr[0]))
        d1 = d.get(chr(arr[1]))
        d2 = d.get(chr(arr[2]))
        d3 = d.get(chr(arr[3]))
        r = d3 | d2 << 4 | d1 << 8 | d0 << 12
        return r


# Описание байтов поля <status>
# Байт S0 – общее состояние
# 7 Всегда равен 1
# 6 Если установлен – Прочие ошибки оборудования
# 5 Если установлен - для вступления в силу изменений настроек необходимо перегрузить
# 4 Если установлен – механизм печатающего устройства не готов к печати (детали Байт S1)
# 3 Если установлен – дисплей не подключен
# 2 Если установлен – Аппарат в аварийном режиме
# 1 Если установлен – буфер документа близок к концу (осталось места на 100 чеков*)
# 0 Если установлен – регистратор находится в сервисном режиме
#
# Байт S1 – состояние принтера
# 7 Всегда равен 1
# 6 Если установлен - Буфер принтера не пуст (есть еще не распечатанные данные)
# 5 Если установлен – осуществляется ручная протяжка бумаги
# 4 Если установлен – открыта крышка принтера
# 3 Если установлен – закончилась контрольная лента
# 2 Если установлен – закончилась чековая лента
# 1 Если установлен – заканчивается контрольная лента
# 0 Если установлен – заканчивается чековая лента
#
# Байт S2 – состояние чека
# 7 Всегда равен 1
# 6 Не определено
# 5 Не определено
# 4 Установлен в 1 при:
# 1. Открытом возвратном чеке.
# 2. При закрытом чеке, если предыдущий документ был возвратным чеком*.
# 3
# Текущее состояние чека (см. раздел «Состояние чека»)
# 2
# 1
# 0
#
# Байт S3 – Дополнительное
# 7 Всегда равен 1
# 6 Не определен
# 5 Не определен
# 4 Не определен
# 3 Не определен
# 2 автономный режим работы
# 1 Если установлен – ошибка карты памяти
# 0 Если установлен – ошибка модуля безопасности
#
# Байт S4 – фискальная память
# 7 Всегда равен 1
# 6 Если установлен – возникла ошибка фискальной памяти
# 5 Если установлен – регистратор персонализирован
# 4 Если установлен – ID_DEV запрограммирован
# 3 Если установлен – фискальная память переполнена
# 2 Если установлен – в ФП места менее чем на 30 отчетов
# 1 Если установлен – регистратор фискализирован
# 0 Если установлен – заводской номер запрограммирован
#
# Байт S5 –Состояние Смены
# 7 Всегда равен 1
# 6 Если установлен – до конца смены(24часа) осталось меньше 60 минут
# 5 Если установлен - часы нуждаются в корректировке
# 4 Если установлен - смена открыта
# 3 Если установлен - Сработало оповещение
# 2 Если установлен - Низкий заряд батареи**
# 1 Не определен
# 0 Не определен


class StatusGeneral:
    # 7 Всегда равен 1
    # 6 Если установлен – Прочие ошибки оборудования
    # 5 Если установлен - для вступления в силу изменений настроек необходимо перегрузить
    # 4 Если установлен – механизм печатающего устройства не готов к печати (детали Байт S1)
    # 3 Если установлен – дисплей не подключен
    # 2 Если установлен – Аппарат в аварийном режиме
    # 1 Если установлен – буфер документа близок к концу (осталось места на 100 чеков*)
    # 0 Если установлен – регистратор находится в сервисном режиме
    def __init__(self):
        self._error_other = False
        self._need_reboot = False
        self._device_print_error = False
        self._display_not_connect = False
        self._device_error = False
        self._buffer_full = False
        self._service_mode = False


if __name__ == '__main__':
    arr = [48, 48, 48, 48]
