class LogInterface:
    def debug(self, *args, **kwargs):
        pass

    def info(self, *args, **kwargs):
        pass

    def warning(self, *args, **kwargs):
        pass

    def error(self, *args, **kwargs):
        pass


class LogPrint(LogInterface):
    def debug(self, *args, **kwargs):
        print("debug: {}".format(args))

    def info(self, *args, **kwargs):
        print("info: {}".format(args))

    def warning(self, *args, **kwargs):
        print("warning: {}".format(args))

    def error(self, *args, **kwargs):
        print("error: {}".format(args))
