# состояний регистратора
# 1 Документ закрыт
# 2 Проверка наличия индикатора
# 3 Игнорировать ошибку ФП
# 4 Игнорировать запрет записи в ФП
# 5 Игнорировать аварийный режим
# 6 Только в фискальном режиме
# 7 При закрытой смене
# 8 В сервисном режиме
# 9 Произвольный текстовый документ открыт
# A Обновляется чек
# B Команда блокируется в автономном режиме

# Код:название команды | Пароль  | Разрешенные состояния
# 0x60:Открытие произвольного текстового документа --345-----B
# 0x61:Печать комментария: текстового --345----AB
# 0x63:Открытие фискального чека -2--------B
# 0x64:Продажа запрограммированного товара -2-------AB
# 0x65:Закрытие чека --345----AB
# 0x67:Оплата -2-------AB
# 0x68:Скидка/наценка на товар процентная -2-------AB
# 0x69:Скидка/наценка на товар абсолютная -2-------AB
# 0x6B:Аннулирования -2-------AB
# 0x6D:Напечатать Итог -2-------AB
# 0x6E:Внесение/выдача сумм -2-------AB
# 0x6F:Печать комментария: штрих кода (bitmap) ---------AB
# 0x71:Печать комментария: штрих кода ---------AB
# 0x70:Выбор отдела -2-------AB
# 0x73:Скидка/наценка на подитог процентная -2-------AB
# 0x74:Скидка/наценка на подитог абсолютная -2-------AB
# 0x75:Скидка с налоговой группы процентная -2-------AB
# 0x76:Скидка с налоговой группы абсолютная -2-------AB
# 0x77:Внесение/ изъятие наличности -2-------AB
# 0x20:Установка даты и времени PROG ----5------
# 0x21:Чтение даты и времени ----5------
# 0x22:Программирование заголовка и окончания документа PROG 1----------
# 0x23:Чтение заголовка и окончания документа -----------
# 0x24:Добавление товара -----------
# 0x25:Поиск индекса товара по коду -----------
# 0x26:Чтение параметров товара по индексу -----------
# 0x27:Программирование параметров регистратора PROG 1-345------
# 0x28:Получение параметров регистратора --345------
# 0x2B:Установить пароли режимов 1----------
# 0x2D:Установка параметров оплаты PROG ------7----
# 0x2E:Чтение параметров оплаты -----------
# 0x2F:Установка параметров оператора PROG 1----------
# 0x30:Чтение параметров оператора -----------
# 0x2C:Удалить оператора PROG 1----------
# 0x29:Подготовить логотип пользователя PROG ------7----
# 0x31:Запрограммировать логотип пользователя PROG ------7----
# 0x32:Добавить отдел PROG -----------
# 0x33:Считать параметры отдела -----------
# 0x34:Добавить во временный буфер текстовую строку -----------
# 0x35:Установка Напоминания SERV -----------
# 0x36:Чтение Напоминания -----------
# 0x37:Чтение параметров товара по Коду -----------
# 0x40:Фискализация FISC ------7----
# 0x41:Программирование заводского номера PROG ------7----
# 0x42:Чтение заводского номера -----------
# 0x43:Программирование фискального номера FISC ------7----
# 0x44:Чтение фискального номера -----------
# 0x45:Программирование налогового/идентификационного номера FISC ------7----
# 0x46:Чтение налогового/идентификационного номера -----------
# 0x47:Установка налоговых ставок FISC ------7----
# 0x48:Чтение налоговых ставок -----------
# 0x49:Установка позиции десятичной точки денежной единицы FISC 1-----7----
# 0x4A:Чтение позиции десятичной точки денежной единицы ---4-------
# 0x4B:Программирование ID_DEV PROG ------7----
# 0x4C:Чтение ID_DEV -----------
# 0x80:Версия ПО --345------
# 0x81:Вывод информации на дисплей -2345-----B
# 0x82:Звук --345------
# 0x83:Открытие денежного ящика --345------
# 0x84:Печать Служебных отчетов ---45---9-B
# 0x85:Пропуск строк 1-345-----B
# 0x86:Отрезать чек 1-345-----B
# 0x88:Установить служебный счетчик PROG --345------
# 0x89:Прочитать служебный счетчик PROG --345------
# 0xA0:Информация о свободных ресурсах и статусе ---45------
# 0xA1:Печать дневных отчетов REPR 1---------B
# 0xA2:Печать периодического отчета по дате FISC ---456--9-B
# 0xA3:Печать периодического отчета по номеру FISC ---456--9-B
# 0xA4:Отчет по реализованным товарам REPR ---4----9--
# 0xA5:Копия фискальных чеков по сквозным номерам REPR ---4----9-B
# 0xA6:Печать дополнительных дневных отчетов REPR ---4----9-B
# 0xE0:Суммы за день -----------
# 0xE1:Суммы по чеку -----------
# 0xE2:Счетчики дневные -----------
# 0xE7:TMPB:Установить источник данных во временном буфере --345------
# 0xE8:TMPB:Прочитать из временного буфера --345------
# 0xE9:TMPB:Записать во временный буфера --345------
# 0xF0:Стереть flash --345--8---
# 0xF1:Считать flash ADM --345------
# 0xF2:Записать flash --345--8---
# 0xF3:Перезапуск устройства --345------
# 0xF4:Отладочн_е функции --345--8---
# 0xF5:Прочитать информацию пользователя ADM --345------
# 0xF6:Записать информацию пользователя ADM --345------
# 0xFA:Прочитать настройки сети ADM -----------
# 0xFB:Записать настройки сети ADM -----------


# Пример 1:
# Команда 0xA0 печать X,Z отчета
# Для того чтоб она выполнилась необходимо:
# 1. Пароль соответствовал паролю отчетов
# 2. Документ закрыт
# 3. Принтер был готов к печати
# Пример 2:
# Команда 0x64 Продажа товара
# Для того чтоб она выполнилась необходимо:
# 1. Пароль соответствовал паролю оператора
# 2. Документ открыл
# 3. Принтер был готов к печати
# + проверка по состоянию чека
# 4. чек должен находится в одном из состояний:
# a. Отпечатан заголовок
# b. Выполнена продажа
# c. Напечатан итог / начаты оплаты
# d. Скидка надбавка на сумму
# e. Выполнено аннулирование
# f. Начато аннулирование
# Пример 3:
# Команда A2h: Печать периодического отчета по дате
# Для того чтоб она выполнилась необходимо:
# 1. Пароль соответствовал паролю отчетов
# 2. Принтер был готов к печати
# 3. Игнорирует запрет записи в ФП
# 4. Игнорирует ошибку часов
# 5. Принтер в фискальном режиме
# 6. Открыт произвольный текстовый документ


# entity = PkgRequestEntity(0x60, '000000', ';')
# await transport.write_read(2, entity)
# entity = PkgRequestEntity(0x61, '000000', ';Test;')
# await transport.write_read(3, entity)
#
# entity = PkgRequestEntity(0x65, '000000', ';0;')
# await transport.write_read(4, entity)
import datetime

from app.device.cr.krypton_transport import PkgRequestEntity, KryptonTransportInterface
from app.device.cr.utils import codec_cp1251


class CommandInterface:

    def cmd(self) -> int:
        raise Exception("NotImplementedException")

    def data(self) -> str:
        raise Exception("NotImplementedException")


class BaseCommand(CommandInterface):

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__()
        self._transport = transport
        self._pwd = '000000'
        self._seq = 1

    @property
    def password(self):
        return self._pwd

    @password.setter
    def password(self, val):
        self._pwd = val

    @property
    def seq(self):
        return self._seq

    @seq.setter
    def seq(self, val):
        self._seq = val

    def exchange_data(self):
        data = self.data()
        pkg = PkgRequestEntity(self.cmd(), self._pwd, data)
        res = self._transport.exchange_data(seq=self._seq, pkg_entity=pkg)
        return res

    def data_to_args(self, data: []):
        d = []
        s = ''
        for b in data:
            if b == 59 and s != '':
                d.append(s)
                s = ''
            elif b != 59:
                s += codec_cp1251[b]
        return d


class TextDocumentOpen(BaseCommand):

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)

    def cmd(self):
        return 0x60

    def data(self):
        return ''


class PrintComment(BaseCommand):
    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)
        self._txt = ''

    @property
    def txt(self):
        return self._txt

    @txt.setter
    def txt(self, v):
        self._txt = v

    def cmd(self):
        return 0x61

    def data(self):
        return self._txt + ';'


class InvoiceFiscalOpen(BaseCommand):

    # Команда 63h: Открытие фискального чека
    # Аргументы сообщения:

    # Аргументысообщения:
    # 1. Номер оператора       N  2
    # 2. Номер кассы           N  1
    # 3. Тип открываемого чека N  1
    #                                0. Продажа товара
    #                                1. Возврат товара
    #                                2. Чек внесения/изъятия
    #                                3. Продажа валют
    #                                4. Покупка валют

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)
        self._operator_number = 1
        self._cashier_number = 1
        self._type_invoice = 0

    def cmd(self):
        return 0x63

    @property
    def operator_number(self):
        return self._operator_number

    @operator_number.setter
    def operator_number(self, val):
        self._operator_number = val

    @property
    def cashier_number(self):
        return self._cashier_number

    @cashier_number.setter
    def cashier_number(self, val):
        self._cashier_number = val

    @property
    def type_invoice(self):
        return self._type_invoice

    @type_invoice.setter
    def type_invoice(self, val):
        self._type_invoice = val

    def data(self):
        return '{};{};{};'.format(self._operator_number, self._cashier_number, self._type_invoice)


class PurchaseGoods(BaseCommand):
    #
    # Команда 64h: Продажа запрограммированного товара
    # Аргументы сообщения:
    # N Описание Тип Размер Примечание
    # 1. Код товара N 6**
    # 2. Количество Q 4 Не равно 0
    # 3. Цена M 4 Может быть равна 0

    # 2;1.000;15.00;

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)
        self._goods_code = 1
        self._count = 1
        self._price = 0

    def cmd(self):
        return 0x64

    @property
    def goods_code(self):
        return self._goods_code

    @goods_code.setter
    def goods_code(self, val):
        self._goods_code = val

    @property
    def count(self):
        return self._count

    @count.setter
    def count(self, val):
        self._count = val

    @property
    def price(self):
        return self._price

    @price.setter
    def price(self, val):
        self._price = val

    def data(self):
        return '{};{:.3f};{:.2f};'.format(self._goods_code, self._count, self._price)


class InvoiceClose(BaseCommand):
    # Команда 65h: Закрытие чека
    # Аргументы сообщения:
    # 1 Вывод информации о расчетной
    # операции на дисплей пользователя*

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)
        self._is_info_display = '0'

    @property
    def is_info_display(self):
        return self.is_info_display

    @is_info_display.setter
    def is_info_display(self, v: bool):
        if v:
            self.is_info_display = '1'
        else:
            self.is_info_display = '0'

    def cmd(self):
        return 0x65

    def data(self):
        return self._is_info_display + ';'


class Payment(BaseCommand):
    # Команда 67h: Оплата
    # Аргументы сообщения:
    # 1 Тип платежа*    N 1     0- (Количество форм оплаты-1)
    # 2 Сумма платежа   M 4

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)
        self._type = 0
        self._amount = 0

    @property
    def amount(self):
        return self._amount

    @amount.setter
    def amount(self, val):
        self._amount = val

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, val):
        self._type = val

    def cmd(self):
        return 0x67

    def data(self):
        return '{};{:.2f};'.format(self._type, self._amount)


class PaymentBank(BaseCommand):
    # Команда 78h: Безналичная оплата
    # Аргументы сообщения:
    # 1 Тип платежа                     N 1  1- (Количество форм оплаты-1)
    # 2 Сумма платежа                   M 4
    # 3 Идентификатор транзакции (RRN)  S 32

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)
        self._type = 0
        self._amount = 0
        self._transaction = ''

    @property
    def amount(self):
        return self._amount

    @amount.setter
    def amount(self, val):
        self._amount = val

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, val):
        self._type = val

    @property
    def transaction(self):
        return self._transaction

    @transaction.setter
    def transaction(self, val):
        self._transaction = val

    def cmd(self):
        return 0x67

    def data(self):
        return '{};{};{};'.format(self._type, self._amount, self._transaction)


class InstallServiceCounter(BaseCommand):

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)
        self._is_info_display = '0'

    def cmd(self):
        return 0x88

    def data(self):
        return self._is_info_display + ';'

    @property
    def is_info_display(self):
        return self.is_info_display

    @is_info_display.setter
    def is_info_display(self, v: bool):
        if v:
            self.is_info_display = '1'
        else:
            self.is_info_display = '0'


class InvoiceCancel(BaseCommand):
    # Команда 6Bh: Аннулирования
    #   Аргументы сообщения:
    #   1 Операция N 1      0. Весь чек
    #                       1. Начать режим аннулирования
    #                       2. Выполнить аннулирование
    #                       3. Отказаться от аннулирования
    #                       4. Выполнить аннулирование без проверки целостности

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)
        self._type = 0

    def cmd(self):
        return 0x6B

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, val):
        self._type = val

    def data(self):
        return '{};'.format(self._type)


class PrintTotal(BaseCommand):
    # Команда 6Dh: Напечатать Итог
    # Аргументы сообщения:
    # 1 Вывод итога на дисплей пользователя*    N   1
    #                                                      При !=0 „СУМА Х.ХХ”

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)
        self._print_display = 0

    def cmd(self):
        return 0x6D

    @property
    def print_display(self):
        return self._print_display

    @print_display.setter
    def print_display(self, val):
        self._print_display = val

    def data(self):
        return '{};'.format(self._print_display)


class AddGoods(BaseCommand):
    # Команда 24h: Добавление товара
    # Аргументы сообщения:
    # N Описание Тип Размер Примечание
    # 1. Код товара N 6**
    # 2. Наименование товара* X 75
    # 3. Налоговая группа N 1 (тип 1-5)
    # 4. Номер отдела N 1
    # 5. Модификатор H 1 Таблица 3
    # 6. Единица измерения S 5 Обозначение размерности

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)
        self._goods_code = 1
        self._name = ''
        self._tax_group = 0
        self._department = 1
        self._modifier = '00'
        self._measure = ''

    def cmd(self):
        return 0x24

    @property
    def goods_code(self):
        return self._goods_code

    @goods_code.setter
    def goods_code(self, val):
        self._goods_code = val

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, val):
        self._name = val

    @property
    def tax_group(self):
        return self._tax_group

    @tax_group.setter
    def tax_group(self, val):
        self._tax_group = val

    @property
    def department(self):
        return self._department

    @department.setter
    def department(self, val):
        self._department = val

    @property
    def modifier(self):
        return self._modifier

    @modifier.setter
    def modifier(self, val):
        self._modifier = val

    @property
    def measure(self):
        return self._measure

    @measure.setter
    def measure(self, val):
        self._measure = val

    def data(self):
        return '{};{};{};{};{};{};'.format(self._goods_code, self._name, self._tax_group, self._department,
                                           self._modifier, self._measure)


class Space(BaseCommand):
    # #Команда 85h: Пропуск строк
    # 1 Количество строк

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)
        self._rows = 0

    def cmd(self):
        return 0x85

    @property
    def rows(self):
        return self._rows

    @rows.setter
    def rows(self, val):
        self._rows = val

    def data(self):
        return '{};'.format(self._rows)


class DailyReport(BaseCommand):
    # Команда A1h: Печать дневных отчетов
    # Аргументы сообщения
    # 1 Тип отчета N 1
    #                   0. Z-отчет с обнулением контрольной ленты
    #                   1. X-отчет
    #                   2. Обнуление контрольной ленты**
    #                   3.
    #                   4. Копия последнего фискального чека
    #                   5.
    #                   6. Z-отчет без обнуления контрольной ленты

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)
        self._type = 1

    def cmd(self):
        return 0xa1

    def z(self):
        self._type = 0

    def x(self):
        self._type = 1

    def zeroing(self):
        self._type = 2

    def invoice_copy(self):
        self._type = 4

    def z_with_zeroing(self):
        self._type = 6

    def data(self):
        return '{};'.format(self._type)


class DaySums(BaseCommand):
    # Команда E0h: Суммы за день
    # Аргументы сообщения:
    # N |       Описание   |   Тип   | Размер
    # 1.|       Источник   |     N   |  1
    # 2.| Индекс значения  |     N   |  1
    # 3.| Модификатор      |     N   |  2
    #
    # Аргументы ответного сообщения:
    # N | Описание    | Тип | Размер
    # 1.| Результат 1 |   M |   8
    # 2.| Результат 2 |   M |   8
    # Возвращаемая информация в зависимости от параметров запроса
    # Источник |       Индекс значения       | Модификатор |     Результат 1          |     Результат 2
    #   0      | Номер налога ** (тип 1-5)   |      0      | Оборот по продажам       | Оборот по возвратам
    #   1      | Номер налога** (тип 1-5)    |      0      | Налог с продаж           | Налог с возвратов
    #   2      | Номер формы оплаты*         |      0      | Оплаты по продаже        | Оплаты по возврату
    #   3      |       0                     |      0      | Сумма наличности в кассе | Наличность на начало смены
    #   3      |       1                     |      0      | Внесено в кассу          | Выдано из кассы
    #   4      | Номер формы оплаты          | №Оператора  | Оплаты по продаже        | Оплаты по возврату
    #   5      |       0                     | №Оператора  | Внесено в кассу          | Выдано из кассы
    #   6      | Номер налога (тип 6-7)      |      0      | Оборот по продажам       | Оборот по покупкам
    #   6      | Номер налога (тип 7)        |      1      | Выдано на игровое место  | Возвращено с игрового места
    #   7      | Номер налога (тип 8-9)      |      0      | Оборот по выигрышу       | Налог с выигрыша
    #   7      | Номер налога (тип 8-9)      |      1      | Выдано на игровое место  | Возвращено с игрового

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)
        self._source = 0
        self._index = 0
        self._modifier = 0

    @property
    def source(self) -> int:
        return self._source

    @source.setter
    def source(self, v: int):
        self._source = v

    @property
    def index(self) -> int:
        return self._index

    @index.setter
    def index(self, v: int):
        self._index = v

    @property
    def modifier(self) -> int:
        return self._modifier

    @modifier.setter
    def modifier(self, v: int):
        self._modifier = v

    def cmd(self):
        return 0xE0

    def data(self):
        return '{};{};{:02d};'.format(self._source, self._index, self._modifier)


class SetDate(BaseCommand):
    # Команда 20h: Установка даты и времени
    # Аргументы сообщения:
    # N Описание Тип Размер Примечание
    #   1. Новая дата D
    #   2. Новое время T
    # Пример: 30 мая 2006 г. 14:30 передается как "300506;1430;"
    # Аргументы ответного сообщения: нет.

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)
        self._dtime: datetime = None

    @property
    def dtime(self) -> datetime:
        return self._dtime

    @dtime.setter
    def dtime(self, d: datetime):
        self._dtime = d

    def cmd(self):
        return 0x20

    def data(self):
        return self._dtime.strftime("%d%m%y;%H%M;")


class Date(BaseCommand):
    # Команда 21h: Чтение даты и времени
    # Аргументы сообщения: нет.
    # Аргументы ответного сообщения:
    # N Описание Тип Размер Примечание
    # 1. Текущая дата D
    # 2. Текущее время T

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)

    def cmd(self):
        return 0x21

    def data(self):
        return ''

    def data_to_datetime(self, data: []) -> datetime.datetime:
        s = ""
        for b in data:
            s += chr(b)
        d = datetime.datetime(year=int('20' + s[5:7]), month=int(s[3:5]), day=int(s[1:3]),
                              hour=int(s[8:10]), minute=int(s[10:12]))
        return d


class TaxRates(BaseCommand):
    # Команда 48h: Чтение налоговых ставок
    # Аргументы сообщения:
    # N |     Описание   | Тип | Размер | Примечание
    # 1 | Номер налога   |  N  |    1   |    0-4
    #
    # Аргументы ответного сообщения:
    # N  |      Описание              |  Тип  | Размер (байт) | Примечание
    # 1. | Тип налоговой ставки       |   N   |      1        | См. виды налогов «Тип»
    # 2. | Значение налога            |   P   |      2        |  0.00-99.99
    # 3. | Наименование               |   S   |      19       |
    # 4. | Связанная налоговая группа |   N   |      1        |

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)
        self._index = 0

    @property
    def index(self) -> int:
        return self._index

    @index.setter
    def index(self, v: int):
        self._index = v

    def cmd(self):
        return 0x48

    def data(self):
        return '{};'.format(self._index)


class PaymentParams(BaseCommand):
    # Команда 2Eh: Чтение параметров оплаты
    # Аргументы сообщения:
    # Бит Разрешения
    # 0. Разрешение использования при продаже
    # 1. Разрешение использования при возврате
    # 2. Не определено
    # 3. Не определено
    # 4. Не определено
    # 5. Не определено
    # 6. Не определено
    # 7. Разрешение начисления сдачи наличными
    # Datecs CMP-10 Версия 3.20 (Red: 2013/11/28)
    # 35
    # N Описание Тип Размер Примечание
    # 1 Номер формы оплаты N 1 0-9
    # Аргументы ответного сообщения:
    # N Описание Тип Размер
    # (байт) Примечание
    # 1. Маска разрешений H 1 См. таблицу 6
    # 2. Наименование S 20
    #
    #

    def __init__(self, transport: KryptonTransportInterface):
        super().__init__(transport)
        self._index = 0

    @property
    def index(self) -> int:
        return self._index

    @index.setter
    def index(self, v: int):
        self._index = v

    def cmd(self):
        return 0x2e

    def data(self):
        return '{};'.format(self._index)
