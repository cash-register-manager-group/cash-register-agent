import time

from serial import Serial

from app.device.cr.errors import KryptonDeviceError, ConnectError
from app.device.cr.pkg_response import PkgResponseEntity

SOH = 0x01
ENQ = 0x05
EOT = 0x04
ETX = 0x03
NAK = 0x15
SYN = 0x16


class PkgRequestEntity:
    def __init__(self, cmd=None, pwd=None, data=None):
        self.cmd = cmd
        self.data = data
        self.pwd = pwd

    @property
    def cmd(self):
        return self._cmd

    @cmd.setter
    def cmd(self, v):
        if isinstance(v, list):
            self._cmd = v
        else:
            self._cmd = [v]

    @property
    def pwd(self):
        return self._pwd

    @pwd.setter
    def pwd(self, s: str):
        if len(s) == 6:
            self._pwd = list(bytearray(s, 'cp1251'))
        else:
            raise RuntimeError("password length must be 6 symbol")

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, v):
        if not isinstance(v, list):
            self._data = list(bytes(v, 'cp1251'))
        else:
            self._data = v

    def __repr__(self):
        return "PkgRequestEntity: cmd={}, pwd={}, data={}" \
            .format(self._cmd, self._pwd, self._data)


class KryptonTransportInterface:
    def exchange_data(self, seq: int, pkg_entity: PkgRequestEntity) -> PkgResponseEntity:
        raise Exception("NotImplementedException")


class KryptonTransport(KryptonTransportInterface):
    READ_MAX_TIMES = 1000

    def __init__(self, serial: Serial):
        self._serial: Serial = serial
        self._serial.timeout = 0.5

    def read_pkg(self) -> PkgResponseEntity:
        bb = []
        i = 0
        empty_count = 0
        while True:
            i += 1
            b = self._serial.read()
            # print('b = {}'.format(b))
            if len(b) == 0:
                if empty_count > 5:
                    i = KryptonTransport.READ_MAX_TIMES
                else:
                    empty_count += 1
                    time.sleep(0.5)
            else:
                b = int.from_bytes(b, "little")
                if b == NAK:
                    raise ConnectError("CRC error in a receive package")
                if b == SYN:
                    i = 0
                    empty_count = 0
                else:
                    bb.append(b)
            if i >= KryptonTransport.READ_MAX_TIMES:
                raise RuntimeError("maximum number of attempts")
            if b == ETX:
                return PkgReceiver(bb).parse()

    def write_pkg(self, data):
        n: int = self._serial.write(data)
        return n

    def exchange_data(self, seq: int, pkg_entity: PkgRequestEntity) -> PkgResponseEntity:
        pkg_response: PkgResponseEntity = self._exchange_data(seq, pkg_entity)
        if seq == pkg_response.seq:
            return self._exchange_data_return(pkg_response, seq)
        else:
            self._serial.reset_input_buffer()
            self._serial.reset_output_buffer()
            pkg_response: PkgResponseEntity = self._exchange_data(seq, pkg_entity)
            return self._exchange_data_return(pkg_response, seq)

    def _exchange_data(self, seq: int, pkg_entity: PkgRequestEntity) -> PkgResponseEntity:
        pkg_request = PkgRequest(seq, pkg_entity).build()
        self.write_pkg(pkg_request)
        pkg_response: PkgResponseEntity = self.read_pkg()
        return pkg_response

    def _exchange_data_return(self, pkg_response, seq):
        if seq == pkg_response.seq:
            if pkg_response.error_code == 0:
                return pkg_response
            else:
                raise KryptonDeviceError(pkg_response.error_code)
        else:
            raise RuntimeError("The seq is error")


class PkgRequest:
    # < SOH > < len > < seq > < cmd > < pwd > < data > < ENQ > < bcc > < ETX >

    def __init__(self, seq, pkg_entity: PkgRequestEntity):
        if (seq < 0) or (seq > 223):
            raise RuntimeError('PkgRequest.__init__: seq={} but must be in range(0, 223)'.format(seq))
        self._seq = seq + 0x20
        self._pkg_entity: [bytes] = pkg_entity

    def build(self):
        pwd = self._pkg_entity.pwd + [0x3b]
        data = self._pkg_entity.cmd + pwd + self._pkg_entity.data
        pkg_len = len(data) + 0x23
        body = [pkg_len] + [self._seq] + data + [ENQ]
        crc = Bcc(body).crc()
        r = [SOH] + body + crc + [ETX]
        # print(''.join('{:02x} '.format(x) for x in res))
        return r


class PkgReceiver:
    # < SOH ><len><seq><cmd><error code><data><EOT><status><ENQ><bcc><ETX>

    def __init__(self, data_bytes: []):
        self._data = data_bytes

    def parse(self):
        idx_soh, idx_eot, idx_enq, idx_etx = self._get_idx()
        body_len = idx_enq - idx_soh
        if body_len <= 0:
            raise RuntimeError('calc body len is incorrect')
        body = self._data[(idx_soh + 1):idx_enq]
        crc = self._data[(idx_enq + 1): (idx_enq + 5)]
        if not self._check_crc(body, crc):
            raise RuntimeError('CRC error')
        len_from_data = self._data[idx_soh + 1] - 0x20
        if len_from_data != body_len:
            raise RuntimeError('len is incorrect')
        entity = PkgResponseEntity()
        entity.len = self._data[idx_soh + 1]
        entity.seq = self._data[idx_soh + 2] - 0x20
        entity.cmd = self._data[idx_soh + 3]
        entity.error_code = self._data[idx_soh + 4: idx_soh + 8]
        entity.status = self._data[idx_soh + 9: idx_soh + 15]
        entity.data = self._data[idx_soh + 8: idx_eot]
        return entity

    def _get_idx(self):
        idx_soh = self._find_idx(self._data, SOH)
        if idx_soh == -1:
            raise RuntimeError('SOH not found')
        idx_eot = self._find_idx(self._data, EOT)
        if idx_eot == -1:
            raise RuntimeError('EOT not found')
        idx_enq = self._find_idx(self._data, ENQ)
        if idx_enq == -1:
            raise RuntimeError('ETX not found')
        idx_etx = self._find_idx(self._data, ETX)
        if idx_etx == -1:
            raise RuntimeError('ETX not found')
        if (idx_etx - idx_enq) != 5:
            raise RuntimeError('ETX is incorrect')
        return idx_soh, idx_eot, idx_enq, idx_etx

    def _check_crc(self, body, crc):
        if len(crc) != 4:
            return False
        calc_crc = Bcc(body + [ENQ]).crc()
        for i in range(4):
            if calc_crc[i] != crc[i]:
                return False
        return True

    def _find_idx(self, arr, val):
        for i in range(len(arr)):
            if val == arr[i]:
                return i
        return -1


class Bcc:
    def __init__(self, list_of_bytes: []):
        self._list_of_bytes = list_of_bytes

    def crc(self):
        crc = 0
        for b in self._list_of_bytes:
            crc += int(b)
        bb = crc.to_bytes(2, 'big')
        b1 = ((bb[0] >> 4) & 0xf) + 0x30
        b2 = (bb[0] & 0xf) + 0x30
        b3 = ((bb[1] >> 4) & 0xf) + 0x30
        b4 = (bb[1] & 0xf) + 0x30
        return [b1, b2, b3, b4]


if __name__ == '__main__':
    print(f"started at {time.strftime('%X')}")
    # transport = KryptonTransport(Serial(port='COM23'))
    # transport = KryptonTransportInterface()
    # comm = commands.TextDocumentOpen(transport)
    # res = comm.exchange_data()

    # entity = PkgRequestEntity(0x88, '000000', ';20;0;')
    # entity.pwd = '000000'
    # entity.cmd = 0x88
    # entity.data = ';20;0;'

    # transport.exchange_data(1, entity)

    # KryptonWorker(transport, 2).print_comment("Hello test")
    # print(f"finished at {time.strftime('%X')}")

    # KryptonWorker(transport, seq=1).test()
    # print(f"finished at {time.strftime('%X')}")

    # bb = [0x22, 0x80, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x3B, 0x05]
    # r = Pkg(bb).forSend()
    # print(r)
    # print("hex- 0x{0:02x}".format(r))
    # print("hex2- {}".format(r.to_bytes(2, 'big')))
    #
    # icrc = r.to_bytes(2, 'big')
    # print(to2byte(icrc[0]))
    # print(to2byte(icrc[1]))

    # for i in range(255):
    #     h = i.to_bytes(4, 'little')
    #     # print("{} - {}".format(i, h))
    #     print_bytes(h)

    # for i in range(256):
    #
    #     print(to2byte(i))

    # br = [0x01, 0x7E, 0x22, 0x80, 0x30, 0x30, 0x30, 0x30, 0x3B, 0x44, 0x61, 0x74, 0x65, 0x63, 0x73, 0x20, 0x65, 0x46,
    #       0x50, 0x2D, 0x32, 0x34, 0x38, 0x30, 0x3B, 0x65, 0x46, 0x50, 0x2D, 0x32, 0x34, 0x38, 0x30, 0x20, 0x35, 0x2E,
    #       0x30, 0x30, 0x3B, 0xD3, 0xEA, 0xF0, 0xE0, 0xBF, 0xED, 0xE0, 0x3B, 0x4A, 0x75, 0x6C, 0x20, 0x20, 0x37, 0x20,
    #       0x32, 0x30, 0x32, 0x30, 0x3B, 0x33, 0x32, 0x33, 0x31, 0x38, 0x3B, 0x33, 0x31, 0x39, 0x30, 0x3B, 0x37, 0x31,
    #       0x36, 0x38, 0x3B, 0x34, 0x34, 0x38, 0x3B, 0x34, 0x32, 0x3B, 0x39, 0x3B, 0x31, 0x30, 0x3B, 0x04, 0x80, 0x80,
    #       0x80, 0x80, 0x91, 0x88, 0x05, 0x31, 0x3B, 0x3E, 0x3C, 0x03]
    # rec = PkgReceiver(br)
    # print(rec.parse())
