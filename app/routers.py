from flask_restful import Api

from app.views import ReportX, ReportZ, FiscalCheck, DTime, CashInBox, Payments


def add_routers(api: Api):
    api.add_resource(FiscalCheck, '/fiscal_check/')
    api.add_resource(ReportX, '/report_x/')
    api.add_resource(ReportZ, '/report_z/')
    api.add_resource(DTime, '/dtime/')
    api.add_resource(CashInBox, '/cash_in_box/')
    api.add_resource(Payments, '/payments/')
