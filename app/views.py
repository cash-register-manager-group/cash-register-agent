from flask import current_app as app
from flask_restful import Resource, reqparse
from werkzeug.exceptions import BadRequest, InternalServerError

from app.device.cr.errors import KryptonDeviceError
from app.service import CashRegister


# amount is in kopecks
class FiscalCheck(Resource):
    TAG = 'FiscalCheck'
    AMOUNT = 'amount'
    GOODS_NAME = 'goods_name'
    GOODS_CODE = 'goods_code'
    TEXT = 'text'
    IS_BANK = 'is_bank'

    def post(self):
        try:
            args = self._get_arguments()
            app.logger.info('{}: args={}'.format(self.TAG, args))
            cash_register = CashRegister.get_instance(log=app.logger)
            amount_hryvnia = args[self.AMOUNT] / 100
            cash_register.payment(amount_hryvnia=amount_hryvnia,
                                  goods_name=args[self.GOODS_NAME],
                                  goods_code=args[self.GOODS_CODE],
                                  is_bank=args[self.IS_BANK],
                                  txt=args[self.TEXT])
            app.logger.info('{}: Ok'.format(self.TEXT))
            return {'result': 'Ok'}
        except KryptonDeviceError as err:
            app.logger.warning('{}: KryptonDeviceError - {}'.format(self.TAG, err))
            e = InternalServerError()
            e.data = {'result': 'Error', 'code': err.args[0][0], 'message': err.args[0][1], }
            raise e
        except Exception as err:
            app.logger.warning('{}: Exception - {}'.format(self.TAG, err))
            e = BadRequest()
            e.data = {'result': 'Error', 'code': 1, 'message': err.args, }
            raise e

    def _get_arguments(self):
        parser = reqparse.RequestParser()
        parser.add_argument(self.AMOUNT, type=int)
        parser.add_argument(self.GOODS_NAME, type=str)
        parser.add_argument(self.GOODS_CODE, type=int)
        parser.add_argument(self.IS_BANK, type=bool)
        parser.add_argument(self.TEXT, type=str)
        args = parser.parse_args()
        return args


class ReportZ(Resource):
    TAG = 'ReportZ'

    def post(self):
        try:
            app.logger.info('{}: staring ...'.format(self.TAG))
            cash_register = CashRegister.get_instance(log=app.logger)
            cash_register.report_z()
            return {'result': 'Ok'}
        except KryptonDeviceError as err:
            app.logger.warning('{}: KryptonDeviceError - {}'.format(self.TAG, err))
            e = InternalServerError()
            e.data = {'result': 'Error', 'code': err.args[0][0], 'message': err.args[0][1], }
            raise e
        except Exception as err:
            app.logger.warning('{}: exception - {}'.format(self.TAG, err))
            e = BadRequest()
            e.data = {'result': 'Error', 'code': 1, 'message': err.args, }
            raise e


class ReportX(Resource):
    TAG = 'ReportX'

    def post(self):
        try:
            app.logger.info('{}: staring ...'.format(self.TAG))
            cash_register = CashRegister.get_instance(log=app.logger)
            cash_register.report_x()
            app.logger.info('{}: Ok'.format(self.TAG))
            return {'result': 'Ok'}
        except KryptonDeviceError as err:
            app.logger.warning('{}: KryptonDeviceError - {}'.format(self.TAG, err))
            e = InternalServerError()
            e.data = {'result': 'Error', 'code': err.args[0][0], 'message': err.args[0][1], }
            raise e
        except Exception as err:
            app.logger.warning('{}: exception - {}'.format(self.TAG, err))
            e = BadRequest()
            e.data = {'result': 'Error', 'code': 1, 'message': err.args, }
            raise e


class DTime(Resource):
    TAG = 'DTime'

    def get(self):
        try:
            app.logger.info('{}: staring ...'.format(self.TAG))
            cash_register = CashRegister.get_instance(log=app.logger)
            d = cash_register.dtime().strftime("%Y-%m-%d %H:%M:%S")
            app.logger.info('{}: dtime={}'.format(self.TAG, d))
            return {'result': 'Ok', 'datetime': d}
        except KryptonDeviceError as err:
            app.logger.warning('{}: KryptonDeviceError - {}'.format(self.TAG, err))
            e = InternalServerError()
            e.data = {'result': 'Error', 'code': err.args[0][0], 'message': err.args[0][1], }
            raise e
        except Exception as err:
            app.logger.warning('{}: exception - {}'.format(self.TAG, err))
            e = BadRequest()
            e.data = {'result': 'Error', 'code': 1, 'message': err.args, }
            raise e


class CashInBox(Resource):
    TAG = 'CashInBox'

    def get(self):
        try:
            app.logger.info('{}: staring ...'.format(self.TAG))
            cash_register = CashRegister.get_instance(log=app.logger)
            d = cash_register.cash_in_box()
            app.logger.info('{}: cash_in_box={}'.format(self.TAG, d))
            return {'result': 'Ok', 'cash_in_box': d[0], 'cash_at_beginning': d[1]}
        except KryptonDeviceError as err:
            app.logger.warning('{}: KryptonDeviceError - {}'.format(self.TAG, err))
            e = InternalServerError()
            e.data = {'result': 'Error', 'code': err.args[0][0], 'message': err.args[0][1], }
            raise e
        except Exception as err:
            app.logger.warning('{}: exception - {}'.format(self.TAG, err))
            e = BadRequest()
            e.data = {'result': 'Error', 'code': 1, 'message': err.args, }
            raise e


class Payments(Resource):
    TAG = 'Payments'

    def get(self):
        try:
            app.logger.info('{}: staring ...'.format(self.TAG))
            cash_register = CashRegister.get_instance(log=app.logger)
            d = cash_register.payments()
            app.logger.info('{}: cash_in_box={}'.format(self.TAG, d))
            return {'result': 'Ok', 'data': d}
        except KryptonDeviceError as err:
            app.logger.warning('{}: KryptonDeviceError - {}'.format(self.TAG, err))
            e = InternalServerError()
            e.data = {'result': 'Error', 'code': err.args[0][0], 'message': err.args[0][1], }
            raise e
        except Exception as err:
            app.logger.warning('{}: exception - {}'.format(self.TAG, err))
            e = BadRequest()
            e.data = {'result': 'Error', 'code': 1, 'message': err.args, }
            raise e
