import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# LOG_FILE_PATH = '/var/log/cragent.log'
LOG_FILE_PATH = 'cragent.log'
SERVER_IP = '0.0.0.0'
SERVER_PORT = 7654
IS_DEBUG = True

SERIAL_PORT = 'COM4'
